# Simple echo server
---
Simple socket server that is written in C and Assembly using system calls 
and no standard libraries. The goal is to keep it as minimalist as possible.


This simple server for the sake of simplicity can only do `GET /index.html` request 
and serve `index.html` file.
For now, it cannot be rendered on conventional browser (the browser that you know 
and use). The only ways to test it is via `curl` command 
or `lynx` a text-based browser.
