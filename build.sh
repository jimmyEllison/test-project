#!/usr/bin/env sh

gcc \
    -std=c89 -pedantic \
    -s -static -Os -no-pie \
    -nostartfiles \
    -nostdlib \
    -ffreestanding \
    -fno-ident \
    -fno-stack-protector \
    -fdata-sections \
    -fno-unwind-tables \
    -fno-asynchronous-unwind-tables \
    -ffunction-sections \
    -Wl,-n \
    -Wl,--gc-sections \
    -Wl,--build-id=none \
    main.c socket.s \
    -o run &&
strip -R .comment ./run
