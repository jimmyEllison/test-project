.global read
read:
mov $0x0, %rax
syscall
ret

.global write
write:
mov $0x1, %rax
syscall
ret

.global open
open:
mov $0x2, %rax
syscall
ret

.global socket
socket:
mov $0x29, %rax
syscall
ret

.global bind 
bind:
mov $0x31, %rax
syscall
ret

.global listen
listen:
mov $0x32, %rax
syscall
ret

.global accept
accept:
mov $0x2b, %rax
syscall
ret

.global send
send:
mov $0x2c, %rax
syscall
ret

.global recv
recv:
mov $0x2d, %rax
syscall
ret

.global close
close:
mov $0x3, %rax
syscall
ret

.global exit
exit:
mov $0x3c, %rax
syscall
