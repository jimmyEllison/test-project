#define AF_INET 2
#define SOCK_STREAM 1
#define STDOUT 1
#define O_RDONLY 0
#define BUF_LEN 256
#define INADDR_LOOPBACK ((unsigned int) 0x7f000001)

typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

typedef struct {
    uint16_t sin_family;
    uint16_t sin_port;
    uint32_t sin_addr;
    char sin_zero[8];
} sockaddr_in;

extern int read(int fd, const void *buffer, uint32_t buf_len);
extern int write(int fd, const void *buffer, uint32_t buf_len);
extern int open(const char *filename, int flags);
extern int socket(int family, int type, int protocol);
extern int bind(int sockfd, const void *addr, uint32_t addrlen);
extern int listen(int sockfd, int backlog);
extern int accept(int sockfd, const void *addr, uint32_t addrlen);
extern int send(int sockfd, const void *buffer, uint32_t buf_len);
extern int recv(int sockfd, const void *buffer, uint32_t buf_len, int flags);
extern int close(int fd);
extern void exit(int exit_code);

int strlen(const char *s) {
    register const char *p;
    for (p = s; *p; ++p);
    return p - s;
}

int print(const char *s) {
    int size = 0;
    size += write(STDOUT, s, strlen(s));
    size += write(STDOUT, "\n", 1);
    return size;
}

void error(const char *s) {
    print(s);
    exit(1);
}

char *strchr(const char *s, int ch) {
    const char c = ch;
    for (; *s != c; ++s) {
        if (*s == '\0') {
            return 0;
        }
    }
    return (char *)s;
}

void *memset(void *dest, int val, uint32_t len) {
    unsigned char *p = dest;
    while (len-- > 0) {
        *p++ = val;
    }
    return dest;
}

uint16_t htons(uint16_t n) {
    return (((n & 0x00FF) << 8) | ((n & 0xFF00) >> 8));
}

uint32_t htonl(uint32_t n) {
    return (((n & 0xFF000000u) >> 24) | ((n & 0x00FF0000u) >> 8) |
            ((n & 0x0000FF00u) << 8) | ((n & 0x000000FFu) << 24));
}

void _start() {
    sockaddr_in addr;
    int cfd;
    int openfd;
    int bytes_read;
    int bytes_sent;
    char *filename;
    char buffer[BUF_LEN];
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (0 > sockfd) {
        error("Failed to open socket!");
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(8080);
    addr.sin_addr = htonl(INADDR_LOOPBACK);
    if (0 > bind(sockfd, &addr, sizeof(addr))) {
        error("Failed to bind address to socket!");
    }

    if (0 > listen(sockfd, 0)) {
        error("Unable to to listen for incoming connection!");
    }

    cfd = accept(sockfd, 0, 0);
    if (0 > cfd) {
        error("Unable to accept connection!");
    }

    if (0 > recv(cfd, buffer, BUF_LEN, 0)) {
        error("Could not receive data from client!");
    }

    filename = buffer + 5;
    *strchr(filename, ' ') = '\0';
    openfd = open(filename, O_RDONLY);
    memset(buffer, 0, BUF_LEN);

    bytes_read = read(openfd, buffer, BUF_LEN);
    if (0 > bytes_read) {
        error("Could not read the file!");
    }

    bytes_sent = send(cfd, buffer, bytes_read);
    if (0 > bytes_sent) {
        error("Could not send the message!");
    }

    close(openfd);
    close(cfd);
    close(sockfd);
    exit(0);
}
